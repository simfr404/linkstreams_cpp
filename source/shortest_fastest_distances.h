//
// Created by Frédéric Simard on 18-08-29.
//

#ifndef UNTITLED_SHORTEST_FASTEST_DISTANCES_H
#define UNTITLED_SHORTEST_FASTEST_DISTANCES_H

#include <set>
#include <map>
#include <unordered_map>
#include "Link_Streams.h"

class AC_D_F_wrapper{
  public:
    Nodes v;
    float tv;
    std::map<Nodes, std::set<std::pair<float, int>>> distances;
    std::map<Nodes, std::set<std::pair<float, float>>> latencies;
    std::map<Nodes, std::map<float, std::set<std::pair<float, int>>>> ac_dict;

    AC_D_F_wrapper();
    
    AC_D_F_wrapper(Nodes vp, float tvp);

    void set_AC_wD0(std::map<std::pair<Nodes, Nodes>, std::map<float, std::set<std::tuple<float, float, int>>>> D0);

    void set_dist_wDAP(std::map<std::pair<Nodes, Nodes>, std::map<float, std::map<float, int>>> D);

    void filtertimeup(float t);
};

class shortest_fastest_distances
{
  public:
    float lambda;

    Link_Streams link_streams;

    shortest_fastest_distances();

    shortest_fastest_distances(Link_Streams ls);

    std::unordered_map<int, std::set<std::pair<int, float>>> distances_wu(float time, Nodes source);

    void void_distances_wu(float time, Nodes source);

    std::unordered_map<int, std::map<float, std::set<std::pair<float, int>>>>
    distances_eff_opt(float time, Nodes source);

    std::tuple<std::map<Nodes, std::set<std::pair<float, int>>>, std::map<Nodes, std::set<std::pair<float, float>>>, std::map<Nodes, std::map<float, std::set<std::pair<float, int>>>>> distances_eff(float time, Nodes source);

    std::tuple<std::map<std::pair<Nodes, Nodes>, std::map<float, std::map<float, int>>>, std::map<Nodes, AC_D_F_wrapper>> all_pairs_fas_distances_for(int max_time_units);

    void void_all_pairs_fas_distances_posgam();

    void void_distances_eff(float time, Nodes source);

    void void_distances_eff_opt_new_DS(float time, Nodes source);

    std::unordered_map<int, std::map<float, std::map<int, std::pair<float, float>>>> distances_eff_opt_new_DS(float time, Nodes source);
    
    void distances_eff_opt_new_DS_alt(float time, Nodes source);
    
    void
    compute_eat(const std::map<std::pair<Nodes, Nodes>, std::map<float, float>> &A, float t, float tplus,
                std::set<Nodes> &C_i) const;

    float iter_time(float t, std::set<float>::reverse_iterator &time_it);

    float find_min_eat(std::map<std::pair<Nodes, Nodes>, std::map<float, std::set<std::tuple<float, float, int>>>> &D,
                      std::pair<Nodes, Nodes> &puv, float s_uv);
};

#endif //UNTITLED_SHORTEST_FASTEST_DISTANCES_H
