//
// Created by Frédéric Simard on 18-08-30.
//

#include "experiments.h"
#include "ls_generators.h"
#include <iostream>
#include <fstream>
#include <assert.h>
#include <tuple>
#include <iomanip>

using namespace std;

experiments::experiments() {}

experiments::experiments(std::vector<int> n, std::vector<float> b, std::vector<float> p, std::vector<int> s,
                         std::string f) {
    nb_nodes = n;
    up_times = b;
    probs = p;
    seeds = s;
    output_filename = f;
    sep = "\t";
}

void experiments::run_randomgnp_exp_wusfpwallpairs(bool testvalues) {
    float a = 0.0;

    ofstream expfile;
    expfile.open(output_filename);
    if (!expfile.is_open())
        cout << "Error with output file" << endl;
    expfile << "seed" << sep << "nb_nodes" << sep << "nb_times" << sep << " nb_edges" << sep << " a" << sep << " b"
            << sep << " p" << sep << "time_wu" << sep << " time_sfpeff" << sep << " time_allpairs" << endl;
    for (int seed : seeds) {
        for (float b : up_times) {
            for (float p : probs) {
                for (int n : nb_nodes) {
                    Link_Streams ls = ls_generators::generate_random_ls(n, a, b, p, (unsigned int) seed, true);

                    int nbtimes = (int) ls.time_set.size();
                    int nbedges = 0;
                    for (auto e_t : ls.temp_edges)
                        nbedges += e_t.second.size();

                    sf_engine = shortest_fastest_distances(ls);

                    set<tuple<Nodes, float, Nodes, float, int>> eff_dist;
                    map<pair<Nodes, Nodes>, map<float, set<pair<float, int>>>> ap_dist;
                    string errorfile = "../output/errors_sfp_vs_allp.csv";
                    ofstream errfile;
                    errfile.open(errorfile);
                    errfile << "Eff distance" << sep << "AP distance" << sep << "suv" << sep << "source" << sep << "auv"
                            << sep << "dest" << endl;

                    auto start = chrono::high_resolution_clock::now();
                    for (auto source : ls.nodes) {
                        if (testvalues) {
                            tuple<map<Nodes, set<pair<float, int>>>, map<Nodes, set<pair<float, float>>>, map<Nodes, map<float, set<pair<float, int>>>>> tmp = sf_engine.distances_eff(
                                    0.0, source);
                            map<Nodes, set<pair<float, int>>> eff_dist_source = get<0>(tmp);
                            map<Nodes, set<pair<float, float>>> eff_lat_source = get<1>(tmp);
                            map<Nodes, map<float, set<pair<float, int>>>> eff_ac_source = get<2>(tmp);

                            for (auto const &ac_s : eff_ac_source) {
                                Nodes v = ac_s.first;
                                for (auto tmp_dis : ac_s.second) {
                                    float suv = tmp_dis.first;
                                    for (auto auv_duv : tmp_dis.second) {
                                        float auv = auv_duv.first;
                                        int duv = auv_duv.second;
                                        auto lat_it = eff_lat_source[v].rbegin();
                                        while (lat_it->first != auv)
                                            ++lat_it;
                                        float latuv = lat_it->second;

                                        if (auv - suv == latuv) {
                                            auto tup = make_tuple(source, suv, v, auv, duv);
                                            eff_dist.insert(tup);
                                        }
                                    }
                                }
                            }
                            assert(eff_dist.size() > 0);
                        } else
                            sf_engine.distances_eff(0.0, source);
                    }

                    auto finish = chrono::high_resolution_clock::now();
                    chrono::duration<double> elapsed = finish - start;

                    auto start_wu = chrono::high_resolution_clock::now();
                    for (auto source : ls.nodes)
                        sf_engine.distances_wu(0.0, source);
                    auto finish_wu = chrono::high_resolution_clock::now();
                    chrono::duration<double> elapsed_wu = finish_wu - start_wu;

                    auto s_ap = chrono::high_resolution_clock::now();
                    if (testvalues) {
                        auto tup = sf_engine.all_pairs_fas_distances_for(0);
                        map<Nodes, AC_D_F_wrapper> macw = get<1>(tup);
                        for (auto macw_u : macw) {
                            for (auto macw_uv : macw_u.second.ac_dict) {
                                pair<Nodes, Nodes> puv(macw_u.first, macw_uv.first);
                                ap_dist[puv] = macw_uv.second;
                            }
                        }
                        assert(ap_dist.size() > 0);
                    } else
                        sf_engine.all_pairs_fas_distances_for(0);
                    auto f_ap = chrono::high_resolution_clock::now();
                    chrono::duration<double> e_ap = f_ap - s_ap;

                    if (testvalues) {
                        for (auto node_pair : ap_dist) {
                            Nodes source = node_pair.first.first;
                            Nodes dest = node_pair.first.second;
                            for (auto tmp_dist : node_pair.second) {
                                float suv = tmp_dist.first;
                                for (auto av_dv : tmp_dist.second) {
                                    float auv = av_dv.first;
                                    int duv = av_dv.second;
                                    auto ap_tup = make_tuple(source, suv, dest, auv, duv);

                                    if (eff_dist.find(ap_tup) == eff_dist.end()) {
                                        for (auto fixed_eff_dist : eff_dist) {
                                            if (get<0>(fixed_eff_dist) == source and get<2>(fixed_eff_dist) == dest and
                                                get<3>(fixed_eff_dist) == auv) {
                                                int effduv = get<4>(fixed_eff_dist);
                                                errfile << effduv << sep << duv << sep << suv << sep << source << sep
                                                        << auv
                                                        << sep << dest << endl;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    expfile << seed << sep << n << sep << nbtimes << sep << nbedges << sep << a << sep << b << sep << p
                            << sep << elapsed_wu.count() << sep << elapsed.count() << sep << e_ap.count() << sep
                            << endl;
                    errfile.close();
                }
            }
        }
    }
    expfile.close();
}

void experiments::run_randomgnp_exp_wu_vs_allpairs() {
    float a = 0.0;

    ofstream expfile;
    expfile.open(output_filename);
    if (!expfile.is_open())
        cout << "Error with output file" << endl;
    expfile << "seed" << sep << "nb_nodes" << sep << "nb_times" << sep << " nb_edges" << sep << " a" << sep << " b"
            << sep << " p" << sep << "time_wu" << sep << "time_eff_opt" << sep << " time_allpairs" << endl;
    for (int seed : seeds) {
        for (float b : up_times) {
            for (float p : probs) {
                for (int n : nb_nodes) {
                    Link_Streams ls = ls_generators::generate_random_ls(n, a, b, p, (unsigned int) seed, true);

                    int nbtimes = (int) ls.time_set.size();
                    int nbedges = 0;
                    for (auto e_t : ls.temp_edges)
                        nbedges += e_t.second.size();

                    sf_engine = shortest_fastest_distances(ls);

                    auto start_wu = chrono::high_resolution_clock::now();
                    for (auto source : ls.nodes)
                        sf_engine.distances_wu(0.0, source);
                    auto finish_wu = chrono::high_resolution_clock::now();
                    chrono::duration<double> elapsed_wu = finish_wu - start_wu;

                    auto start_opt = chrono::high_resolution_clock::now();
                    for (auto source : ls.nodes)
                        sf_engine.distances_eff_opt(0.0, source);
                    auto f_opt = chrono::high_resolution_clock::now();
                    chrono::duration<double> e_opt = f_opt - start_opt;

                    auto s_ap = chrono::high_resolution_clock::now();
                    sf_engine.all_pairs_fas_distances_for(0);
                    auto f_ap = chrono::high_resolution_clock::now();
                    chrono::duration<double> e_ap = f_ap - s_ap;

                    expfile << seed << sep << n << sep << nbtimes << sep << nbedges << sep << a << sep << b << sep << p
                            << sep << elapsed_wu.count() << sep << e_opt.count() << sep << e_ap.count() << sep << endl;
                }
            }
        }
    }
    expfile.close();
}

void
experiments::run_inputls_exp(std::string inputfile, bool verbose, int verbose_num, int exp_mode, float input_lambda,
                             bool append, int repeat_exp, int nbsources, vector<int> sfd_methods) {
    Link_Streams ls;
    ofstream expfile;
    sf_engine = shortest_fastest_distances();
    vector<Nodes> sources_set;

    initialize_exps(inputfile, verbose, input_lambda, append, nbsources, ls, expfile, sources_set);

    int n = ls.nodes.size();
    int nbtimes = ls.time_set.size();
    int nbedges = 0;
    for (auto t_e : ls.temp_edges)
        nbedges += t_e.second.size();

    chrono::duration<double> elapsed;
    string name;

    for (int i = 0; i < repeat_exp; i++) {
        for (int xp : sfd_methods) {
            std::cout << "Launching method " << xp << endl;
            int count = 0;
            int size_L_wu = 0;
            int size_ac_o = 0;
            int size_ac_ds = 0;
            auto start = std::chrono::high_resolution_clock::now();
            if (xp < 7) {
                for (auto source : sources_set) {
                    switch (xp) {
                        case 0: {
                            unordered_map<int, set<pair<int, float>>> L = sf_engine.distances_wu(0.0, source);
                            name = "wu";
                            if (exp_mode == 2) {
                                bool isempty = true;
                                for (auto u_d : L)
                                    for (auto d_a : u_d.second) {
                                        size_L_wu++;
                                        isempty = false;
                                    }
                                assert(isempty == false);
                            }
                            break;
                        }
                        case 2:
                            sf_engine.distances_eff(0.0, source);
                            name = "eff";
                            break;
                        case 3:
                            sf_engine.void_distances_eff_opt_new_DS(0.0, source);
                            name = "eff opt ds";
                            break;
                        case 4:
                            sf_engine.distances_eff_opt_new_DS_alt(0.0, source);
                            name = "eff opt ds alt";
                            break;
                        case 5: {
                            unordered_map<int, map<float, map<int, pair<float, float>>>> AC_ds = sf_engine.distances_eff_opt_new_DS(
                                    0.0, source);
                            name = "eff opt ds";
                            if (exp_mode == 2)
                                for (auto u_s : AC_ds)
                                    for (auto s_d : u_s.second)
                                        size_ac_ds++;
                            break;
                        }
                        case 6:
                            sf_engine.void_distances_wu(0.0, source);
                            name = "wu";
                            break;
                        default: {
                            unordered_map<int, map<float, set<pair<float, int>>>> AC_o = sf_engine.distances_eff_opt(
                                    0.0,
                                    source);
                            name = "eff opt";
                            if (exp_mode == 2)
                                for (auto u_s : AC_o)
                                    for (auto s_d : u_s.second)
                                        for (auto d_set : s_d.second)
                                            size_ac_o++;
                            break;
                        }
                    }

                    if (verbose) {
                        if (count % verbose_num == 0)
                            std::cout << count << " operations completed" << endl;
                    }
                    count++;
                }
            } else {
                switch (xp) {
                    case 7:
                        sf_engine.void_all_pairs_fas_distances_posgam();
                        name = "MSMD gamma>0";
                        break;
                    case 8:
                        sf_engine.all_pairs_fas_distances_for(-1);
                        name = "MSMD gamma=0";
                        break;
                    default:
                        sf_engine.void_all_pairs_fas_distances_posgam();
                        name = "MSMD gamma>0";
                        break;
                }
            }
            if (exp_mode == 2 and verbose) {
                switch (xp) {
                    case 0:
                        cout << "Average size of L_wu per source " << size_L_wu / (float) sources_set.size() << endl;
                        break;
                    case 1:
                        cout << "Average size of AC_o per source " << size_ac_o / (float) sources_set.size() << endl;
                        break;
                    case 5:
                        cout << "Average size of AC_ds per source " << size_ac_ds / (float) sources_set.size() << endl;
                        break;
                    default:
                        break;
                }
            }
            auto finish = std::chrono::high_resolution_clock::now();
            elapsed = finish - start;

            if (verbose)
                cout << "Saving values to file" << endl;

            expfile << right << setw(15) << n << setw(15) << nbtimes << setw(15) << nbedges << setw(15)
                    << elapsed.count()
                    << setw(15) << name << endl;
        }
    }
    expfile.close();
}

void experiments::run_random_avgdist(bool verbose, int verbose_num, bool append, int rep_exp) {
    ofstream expfile;
    if (append)
        expfile.open(output_filename, ios_base::app);
    else
        expfile.open(output_filename, ofstream::out);

    expfile << "nb_nodes" << sep << "nb_times" << sep << "nb_edges" << sep << "t" << sep << "d_t" << sep << "df_t"
            << endl;
    auto start_all_exp = chrono::high_resolution_clock::now();
    chrono::duration<double> elapsed_all;

    cout << "Starting all exps" << endl;
    for (int n = 70; n < 100; n += 5) {
        int nbtimes, nbedges;

        Link_Streams ls;

        cout << "Commencing experiments with " << n << " nodes " << endl;
        for (int i = 0; i < rep_exp; i++) {
            cout << "Generating link stream " << endl;
            ls = ls_generators::generate_random_ls(n, 0, 100, 0.7, (unsigned int) 1243214 * i, false);
            cout << "Generating done " << endl;
            nbtimes = ls.time_set.size();
            nbedges = 0;
            for (auto e_t : ls.temp_edges)
                nbedges += e_t.second.size();
            sf_engine = shortest_fastest_distances(ls);
            cout << "Computing distances " << endl;
            auto tup = sf_engine.all_pairs_fas_distances_for(0);
            cout << "Computing done " << endl;
            auto wrap = get<1>(tup);
            auto D = get<0>(tup);
            for (float t : ls.time_set) {
                float d_t = 0.0;
                float df_t = 0.0;
                int denum_t = 0;
                int denumf_t = 0;

                for (Nodes source : ls.nodes) {
                    auto ac = wrap[source].ac_dict;
                    for (auto u : ac)
                        for (auto sv : u.second)
                            for (auto avdv : sv.second) {
                                if (sv.first >= t) {
                                    d_t += avdv.second;
                                    denum_t += 1;
                                }
                            }
                }
                for (Nodes u : ls.nodes)
                    for (Nodes v : ls.nodes)
                        if (D[make_pair(u, v)].find(t) != D[make_pair(u, v)].end())
                            for (auto svd : D[make_pair(u, v)][t]) {
                                df_t += svd.second;
                                denumf_t += 1;
                            }
                d_t /= denum_t;
                df_t /= denumf_t;
                expfile << n << sep << nbtimes << sep << nbedges << sep << t << sep << d_t << sep << df_t << endl;
            }
        }
    }
    expfile.close();
    cout << "Experiments over" << endl;
}

void experiments::run_lsgenerated_allpairs(std::string inputfile, bool verbose, int verbose_num, int exp_mode, float input_lambda, bool append, int repeat_exp, std::tuple<int, int, int> node_lb_ub,
                                           std::vector<int> sfd_methods, int ls_generator, std::pair<int, int> time_lb_ub, std::vector<float> gen_probs, bool realdurations) {

    ofstream expfile;
    ofstream eff_file;
    ofstream ap_file;
    eff_file.open("output/eff_ac_dict_values.csv");
    ap_file.open("output/ap_ac_dict_values.csv");

    if (append)
        expfile.open(output_filename, ios_base::app);
    else
        expfile.open(output_filename, ofstream::out);
    expfile << right << "nb_nodes" << sep << "nb_times" << sep << " nb_edges" << sep
            << "probability" << sep << " time" << sep << " name" << endl;

    auto start_all_exp = std::chrono::high_resolution_clock::now();
    chrono::duration<double> elapsed_all_exp;

    cout << "Starting all exps" << endl;
    for (int n = get<0>(node_lb_ub); n < get<1>(node_lb_ub); n += get<2>(node_lb_ub)) {
        int nbtimes;
        int nbedges;

        Link_Streams ls;

        set<tuple<Nodes, Nodes, float, float, int>> eff_tup;
        set<tuple<Nodes, Nodes, float, float, int>> ap_tup;
        map<Nodes, map<Nodes, map<float, set<pair<float, int>>>>> eff_dist;
        map<Nodes, map<Nodes, map<float, set<pair<float, int>>>>> ap_dist;

        cout << "Commencing exps with " << n << " nodes " << endl;
        for (int i = 0; i < repeat_exp; i++) {
            for (float p : gen_probs) {
                switch (ls_generator) {
                    case 0:
                        ls = ls_generators::generate_random_ls(n, time_lb_ub.first, time_lb_ub.second, p,
                                                               (unsigned int) 12431 * i + i,
                                                               false); //Does not have durations, cannot be used for gamma>0
                        break;
                    case 1:
                        ls = ls_generators::generate_random_ls_withdur(n, time_lb_ub.first, time_lb_ub.second, p,
                                                                       (unsigned int) 12431 * i + i, realdurations);
                        break;
                }

                nbtimes = ls.time_set.size();
                nbedges = 0;
                for (auto e_t : ls.temp_edges)
                    nbedges += e_t.second.size();

                sf_engine = shortest_fastest_distances(ls);
                sf_engine.lambda = input_lambda;

                for (int xp : sfd_methods) {
                    chrono::duration<double> elapsed(0);
                    string name;

                    switch (xp) {
                        case 0: {
                            int count = 0;
                            name = "SSSMD g>0";

                            for (auto source : ls.nodes) {
                                auto start = std::chrono::high_resolution_clock::now();
                                sf_engine.distances_eff_opt(0.0, source);
                                auto finish = std::chrono::high_resolution_clock::now();
                                elapsed = elapsed + finish - start;

                                count++;
                                if (verbose) {
                                    if (count % verbose_num == 0)
                                        std::cout << count << " operations completed" << endl;
                                }
                            }
                            cout << "--------------------" << endl;
                            break;
                        }
                        case 1: {
                            int count = 0;
                            name = "SSSMD g=0";

                            for (auto source : ls.nodes) {
                                auto start = std::chrono::high_resolution_clock::now();
                                auto wrap = sf_engine.distances_eff(0.0, source);
                                auto finish = std::chrono::high_resolution_clock::now();
                                elapsed = elapsed + finish - start;

                                auto ac = get<2>(wrap);
                                for (auto ackey : ac) {
                                    Nodes v = ackey.first;
                                    for (auto ac_v : ackey.second) {
                                        float sv = ac_v.first;
                                        for (auto ac_v_pair : ac_v.second) {
                                            float av = ac_v_pair.first;
                                            int dv = ac_v_pair.second;
                                            eff_file << source << sep << v << sep << sv << sep << av << sep << dv
                                                     << endl;
                                            eff_dist[source][v][sv].insert(pair<float, int>(av, dv));
                                            eff_tup.insert(make_tuple(source, v, sv, av, dv));
                                        }
                                    }
                                }

                                count++;
                                if (verbose) {
                                    if (count % verbose_num == 0)
                                        std::cout << count << " operations completed" << endl;
                                }
                            }
                            cout << "--------------------" << endl;
                            break;
                        }
                        case 2: {
                            name = "MSMD g=0";
                            auto start = std::chrono::high_resolution_clock::now();
                            auto tup = sf_engine.all_pairs_fas_distances_for(-1);

                            auto finish = std::chrono::high_resolution_clock::now();
                            elapsed = finish - start;

                            if (verbose)
                                cout << "All operations completed" << endl;

                            auto wrap = get<1>(tup);
                            for (Nodes source : ls.nodes) {
                                auto ac = wrap[source].ac_dict;

                                for (auto ackey : ac) {
                                    Nodes v = ackey.first;
                                    for (auto ac_v : ackey.second) {
                                        float sv = ac_v.first;
                                        for (auto ac_v_pair : ac_v.second) {
                                            float av = ac_v_pair.first;
                                            int dv = ac_v_pair.second;
                                            ap_file << source << sep << v << sep << sv << sep << av << sep << dv
                                                    << endl;
                                            ap_dist[source][v][sv].insert(pair<float, int>(av, dv));
                                            ap_tup.insert(make_tuple(source, v, sv, av, dv));
                                        }
                                    }
                                }
                            }

                            cout << "---------------------" << endl;
                            break;
                        }
                        case 3: {
                            name = "MSMD g>0";
                            auto start = std::chrono::high_resolution_clock::now();
                            sf_engine.void_all_pairs_fas_distances_posgam();

                            auto finish = std::chrono::high_resolution_clock::now();
                            elapsed = finish - start;

                            if (verbose)
                                cout << "All operations completed" << endl;

                            cout << "---------------------" << endl;
                            break;
                        }
                        default: {
                            name = "MSMD g>0";
                            auto start = std::chrono::high_resolution_clock::now();
                            sf_engine.void_all_pairs_fas_distances_posgam();

                            auto finish = std::chrono::high_resolution_clock::now();
                            elapsed = finish - start;

                            if (verbose)
                                cout << "All operations completed" << endl;

                            cout << "---------------------" << endl;
                            break;
                        }
                    }

                    expfile << right << n << sep << nbtimes << sep << nbedges << sep << p
                            << sep << elapsed.count() << sep << name << endl;
                }
            }
        }
    }
    expfile.close();
    eff_file.close();
    ap_file.close();

    auto finish_all_exp = chrono::high_resolution_clock::now();
    elapsed_all_exp = finish_all_exp - start_all_exp;
    cout << "All experiments finished in time " << elapsed_all_exp.count() << "seconds" << endl;;
}

void experiments::print_fasdis(map<Nodes, map<Nodes, map<float, set<pair<float, int>>>>> &eff_dist,
                               map<Nodes, map<Nodes, map<float, set<pair<float, int>>>>> &ap_dist) {
    for (auto s_d : ap_dist) {
        Nodes s = s_d.first;
        for (auto s_d_v : s_d.second) {
            Nodes v = s_d_v.first;
            for (auto s_d_v_sv : s_d_v.second) {
                float sv = s_d_v_sv.first;
                if (eff_dist[s][v].find(sv) != eff_dist[s][v].end()) {
                    for (auto av_dv : s_d_v_sv.second) {
                        int dv = av_dv.second;
                        if (eff_dist[s][v][sv].find(av_dv) == eff_dist[s][v][sv].end()) {
                            for (auto ap_dp : eff_dist[s][v][sv]) {
                                if (ap_dp.second != dv) {
                                    cout << "--->  " << s << sep << v << sep << sv << sep << av_dv.first << sep
                                         << dv << " //"
                                         << sep << ap_dp.first << sep << ap_dp.second << endl;
                                } else
                                    cout << "===  " << s << sep << v << sep << sv << sep << av_dv.first << sep << dv
                                         << " //"
                                         << sep << ap_dp.first << sep << ap_dp.second << endl;
                            }
                        }
                    }
                } else {
                    for (auto s_d_eff : eff_dist) {
                        if (s_d_eff.first == s) {
                            for (auto s_d_v_eff : s_d_eff.second) {
                                if (s_d_v_eff.first == v) {
                                    for (auto sv_eff : s_d_v_eff.second) {
                                        float sv_e = sv_eff.first;
                                        if (sv_e > sv) {
                                            for (auto av_dv : s_d_v_sv.second) {
                                                if (av_dv.first < numeric_limits<float>::infinity()) {
                                                    for (auto av_dv_eff : sv_eff.second) {
                                                        if (av_dv_eff.first == av_dv.first &&
                                                            av_dv_eff.second != av_dv.second)
                                                            cout << "(" << s << "," << v << ")" << sep
                                                                 << "(" << sv_e << "," << av_dv_eff.first << ")"
                                                                 << sep
                                                                 << "(" << sv << "," << av_dv.first << ")" << sep
                                                                 << av_dv.second << " // " << sep
                                                                 << av_dv_eff.second << endl;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}

void experiments::print_fastup(set<tuple<Nodes, Nodes, float, float, int>> &ef_t,
                               set<tuple<Nodes, Nodes, float, float, int>> &ap_t) {
    cout << "Missing in eff" << endl;
    for (auto ap : ap_t) {
        if (ef_t.find(ap) == ef_t.end()) {
            cout << get<0>(ap) << " ";
            cout << get<1>(ap) << " ";
            cout << get<2>(ap) << " ";
            cout << get<3>(ap) << " ";
            cout << get<4>(ap) << " ";
            cout << endl;
        }
    }
    cout << "---" << endl;
    cout << "Missing in ap" << endl;
    for (auto ef : ef_t) {
        if (ap_t.find(ef) == ap_t.end()) {
            cout << get<0>(ef) << " ";
            cout << get<1>(ef) << " ";
            cout << get<2>(ef) << " ";
            cout << get<3>(ef) << " ";
            cout << get<4>(ef) << " ";
            cout << endl;
        }
    }
}

void experiments::initialize_exps(string inputfile, bool verbose, float input_lambda, bool append, int nbsources,
                                  Link_Streams &ls, ofstream &expfile, vector<Nodes> &sources_set) {
    if (append)
        expfile.open(output_filename, ios_base::app
        );
    else
        expfile.open(output_filename);
    expfile << right << setw(15) << "nb_nodes" << setw(15) << "nb_times" << setw(15) << " nb_edges" << setw(15)
            << " time"
            << setw(15) << "Method" << endl;

    if (verbose)
        cout << "Building link stream " << endl;

    if (inputfile != "")
        ls.read_koblenz_link_stream_from_file(inputfile, input_lambda);
    else
        ls.read_koblenz_link_stream_from_file(input_file, input_lambda);
    if (verbose)
        cout << "Build done " << endl;

    if (verbose)
        cout << "Counting elements " << endl;

    int n = (int) ls.nodes.size();
    int nbtimes = (int) ls.time_set.size();
    int nbedges = 0;
    for (auto e_t: ls.temp_edges)
        nbedges += e_t.second.size();

    if (verbose)
        cout << "Counting done " << endl;

    if (n == 0)
        return;

    sf_engine.link_streams = ls;
    sf_engine.lambda = input_lambda;


    vector<int> indices_picked;
    std::default_random_engine gen(12412);
    std::uniform_int_distribution<int> dist(0, ls.nodes.size());

    if (verbose)
        cout << "Initializing sources " << endl;
    while (sources_set.size() < nbsources and sources_set.size() < ls.nodes.size()) {
        int ind = dist(gen);
        while (find(indices_picked.begin(), indices_picked.end(), ind) != indices_picked.end()) {
            ind = dist(gen);
        }
        indices_picked.push_back(ind);
        set<Nodes>::iterator it = ls.nodes.begin();
        std::advance(it, ind);
        Nodes x = *it;
        sources_set.push_back(x);
    }
    if (verbose) {
        cout << "Initialization done " << endl;
        cout << "Launching distances methods " << endl;
        cout << "----------------------------" << endl;
        cout << "----------------------------" << endl;
    }
}
