//
// Created by Frédéric Simard on 18-08-29.
//
//LS IS ASSUMED DIRECTED
//TODO: ADDING EDGES IN BOTH DIRECTIONS SLOWS DOWN COMPUTATION


#include <fstream>
#include <string>
#include <sstream>
#include <regex>
#include <iostream>
#include <list>
#include <queue>
#include "Link_Streams.h"
#include <math.h>
#include <assert.h>

using namespace std;


Nodes::Nodes(int i) {
    id = i;
}

Edges::Edges(float time, Nodes nu, Nodes nv, float d) : u(nu), v(nv) {
    t = time;
    dur = d;
}


bool Edges::operator==(const Edges &e1) {
    if (t == e1.t && u == e1.u && v == e1.v)
        return true;
    return false;
}

bool Edges::operator<(const Edges &e) {
    if (t < e.t)
        return true;
    return false;
}

Link_Streams::Link_Streams() {}

void Link_Streams::add_temporal_edge(Edges e) {
    float time = e.t;
    float dur = e.dur;

    temp_edges[time].push_back(e);
    time_set.insert(time);

    pair<float, Nodes> tmp_u(time, e.u);
    temp_nodes.insert(tmp_u);
    pair<float, Nodes> tmp_v(time, e.v);
    temp_nodes.insert(tmp_v);

    add_adjac(e, time);
    add_adjac(e, time + dur);

    auto tmpu = nodes.find(e.u);
    auto tmpv = nodes.find(e.v);
    if (tmpu == nodes.end())
        add_node(e.u);
    if (tmpv == nodes.end())
        add_node(e.v);
}

void Link_Streams::add_adjac(const Edges &e, float time) {
    if (temp_adj.find(time) == temp_adj.end()) {
        set<Nodes> tmp_vec{e.v};
        map<Nodes, set<Nodes>> tmp_map;
        tmp_map[e.u] = tmp_vec;
        temp_adj[time] = tmp_map;
    } else {
        if (temp_adj[time].find(e.u) == temp_adj[time].end()) {
            set<Nodes> tmp_vec{e.v};
            temp_adj[time][e.u] = tmp_vec;
        } else
            temp_adj[time][e.u].insert(e.v);
    }
}

void Link_Streams::add_node(Nodes n) {
    nodes.insert(n);
}

Link_Streams::~Link_Streams() {
    if (!nodes.empty())
        nodes.clear();
    if (!temp_edges.empty())
        temp_edges.clear();
    if (!time_set.empty())
        time_set.clear();
}

void Link_Streams::read_normal_link_stream_from_file(std::string filename) {
    ifstream infile(filename);

    string edges;
    if (infile.is_open()) {
        while (getline(infile, edges)) {
            vector<float> temp_edges = split(edges);
            int u = (int) temp_edges.at(0);
            int v = (int) temp_edges.at(1);
            float t = temp_edges.at(2);
            Edges e(t, u, v, 0);
            add_temporal_edge(e);
        }
    }
    infile.close();
}

std::vector<float> Link_Streams::split(const std::string &in_csv) {
    vector<float> elem;
    stringstream ss(in_csv);
    string subs;
    while (getline(ss, subs, sep)) {
        float temp = (float) ::atof(subs.c_str());
        elem.push_back(temp);
    }

    return elem;
}

void Link_Streams::read_koblenz_link_stream_from_file(std::string filename, float use_lambda) {
    ifstream infile(filename);
    string edges;
    if (infile.is_open()) {
        while (getline(infile, edges)) {
            if (edges.find("%") == std::string::npos && !edges.empty()) {
                std::stringstream ss(edges);
                std::vector<float> numbers;
                for (float i = 0.0; ss >> i;) {
                    numbers.push_back(i);
                }
                int u = (int) numbers.at(0);
                int v = (int) numbers.at(1);
                float t = numbers.at(3);
                Edges e(t, u, v, use_lambda);
//                Edges ep(t,v,u,0);
                add_temporal_edge(e);
//                add_temporal_edge(ep);
            }
        }
    } else {
        cout << "File cannot be found" << endl;
    }
    infile.close();
}

std::map<int, std::set<Nodes>> Link_Streams::connected_components(float time, int max_time_unit) {
    map<int, set<Nodes>> C;

    auto start = chrono::high_resolution_clock::now();
    int i = 0;
    map<int, bool> seen;
    for (Nodes s : nodes)
        seen[s.id] = false;

    for (Nodes source : nodes) {
        if (seen[source.id] == false) {

            map<int, bool> visited;
            for (Nodes u : nodes)
                visited[u.id] = false;

            queue<Nodes> queue;
            visited[source.id] = true;
            queue.push(source);

            while (queue.empty() == false) {
                Nodes u = queue.front();
                queue.pop();
                C[i].insert(u);

                for (Nodes v : temp_adj[time][u]) {
                    if (visited[v.id] == false) {
                        visited[v.id] = true;
                        queue.push(v);
                    }
                }
            }
            for (Nodes u: C[i])
                seen[u.id] = true;

            visited.clear();
            i++;
        }

        if (max_time_unit > -1) {
            auto current = chrono::high_resolution_clock::now();
            auto elapsed = chrono::duration_cast<chrono::seconds>(current - start);
            int int_elapsed = chrono::duration<int>(elapsed).count();
            if (int_elapsed >= max_time_unit)
                return C;
        }
    }
//    for (Nodes u : nodes)
//        for (int i = 0; i < C.size(); i++)
//            for (int j = 0; j < C.size(); j++)
//                if (i != j and C[i].find(u) != C[i].end())
//                    if (C[j].find(u) != C[j].end()) {
//                        cout << "Problem here with " << i << j << u << endl;
//                        assert(C[j].find(u) == C[j].end());
//                    }
    return C;
}

std::map<Nodes, std::map<Nodes, int>> Link_Streams::all_pairs_distances(float time) {
    map<Nodes, map<Nodes, int>> dist;

    for (Nodes source: nodes) {
        map<Nodes, int> dis_s;
        map<Nodes, bool> visited;

        for (Nodes v : nodes) {
            if (v == source) {
                visited[v] = true;
                dis_s[v] = 0;
            } else {
                visited[v] = false;
                dis_s[v] = 10 * nodes.size();
            }
        }
        list<Nodes> queue;
        queue.push_back(source);

        while (!queue.empty()) {
            Nodes u = queue.front();
            queue.pop_front();

            for (Nodes v : temp_adj[time][u]) {
                int disv = dis_s[u] + 1;
                if (disv < dis_s[v])
                    dis_s[v] = disv;
                if (!visited[v]) {
                    visited[v] = true;
                    queue.push_back(v);
                }
            }
        }

        dist[source] = dis_s;
    }

    return dist;
}

Nodes::Nodes() {}

bool operator<(Nodes n1, Nodes n2) {
    return n1.id < n2.id;
}

bool operator==(Nodes n1, Nodes n2) {
    return n1.id == n2.id;
}

bool operator!=(Nodes n1, Nodes n2) {
    return !(n1 == n2);
}

ostream &operator<<(ostream &os, const Nodes &n) {
    os << n.id;
    return os;
}

ostream &operator<<(ostream &os, const Edges &e) {
    os << "( " << e.t << ", " << e.u << ", " << e.v << ", " << e.dur << " )";
    return os;
}

void Link_Streams::print_tempedges() {
    for (auto e_t : temp_edges)
        for (auto e : e_t.second)
            std::cout << e << std::endl;
}

void Link_Streams::print_adjlist() {
    for (auto t_adj : temp_adj) {
        float t = t_adj.first;
        for (auto t_adj_s : t_adj.second) {
            Nodes s = t_adj_s.first;
            std::cout << t << ", " << s << ":";
            for (auto v : t_adj_s.second)
                std::cout << " " << v;
            std::cout << std::endl;
        }
    }
}

void Link_Streams::add_ghost_nodes() {
    for (float t : time_set) {
        for (Nodes v : nodes) {
            pair<float, Nodes> tv(t, v);
            if (temp_nodes.find(tv) == temp_nodes.end())
                temp_nodes.insert(tv);
        }
    }
}

void Link_Streams::add_ghost_edges() {
    for (float t : time_set) {
        for (Edges e : temp_edges[t]) {
            float d = e.dur;
            set<float> tmp(time_set.lower_bound(t), time_set.upper_bound(t + d));
            for (float tp : tmp) {
                if (time_set.find(tp) == time_set.end())
                    time_set.insert(tp);
                if (find(temp_edges[tp].begin(), temp_edges[tp].end(), e) == temp_edges[tp].end()) {
                    Edges ne(tp, e.u, e.v, tp + d - tp);
                    add_temporal_edge(ne);
                }
            }
        }
    }
}

