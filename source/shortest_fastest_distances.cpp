//
// Created by Frédéric Simard on 18-08-29.
//

#include <iostream>
#include "shortest_fastest_distances.h"
#include <iostream>
#include <assert.h>
#include <queue>

using namespace std;

size_t name_hash(const Nodes &k) {
    // Compute individual hash values for two data members and combine them using XOR and bit shifting
    return hash<int>()((k.id));
}

shortest_fastest_distances::shortest_fastest_distances() {}

shortest_fastest_distances::shortest_fastest_distances(Link_Streams ls) { link_streams = ls; }

//TODO:doit avoir un bug, ma methode peut pas etre plus rapide que Wu
//TODO:possiblement que avec lambda > 0, je dois enlever les elements domines dans AC
unordered_map<int, set<pair<int, float>>>
shortest_fastest_distances::distances_wu(float time, Nodes source) {
    unordered_map<int, set<pair<int, float>>> L;
    map<Nodes, float> d;
    for (auto v : link_streams.nodes) {
        L[v.id] = set<pair<int, float>>();
        d[v] = v == source ? 0 : numeric_limits<float>::infinity();
    }

    for (auto t : link_streams.time_set) {
        if (t >= time) {
            for (auto e : link_streams.temp_edges[t]) {
                Nodes u = e.u;
                Nodes v = e.v;
                float dur = e.dur;
                if (dur >= lambda) {
                    if (u == source) {
                        pair<int, float> source_dist(0, t);
                        if (L[source.id].find(source_dist) == L[source.id].end())
                            L[source.id].insert(source_dist);
                    }

                    if (L[u.id].begin() != L[u.id].end()) {
                        auto it_dis_u = L[u.id].rbegin();

                        float au = it_dis_u->second;
                        int du = it_dis_u->first;
                        ++it_dis_u;
                        while (au > t and it_dis_u != L[u.id].rend()) {
                            au = it_dis_u->second;
                            du = it_dis_u->first;
                            ++it_dis_u;
                        }
                        if (au <= t) {
                            int d_v = du + 1;
                            float a_v = t + lambda;
                            pair<int, float> dis_v(d_v, a_v);
                            L[v.id].insert(dis_v);

                            vector<pair<int, float>> to_remove;
                            for (auto dis_p : L[v.id]) {
                                int dp = dis_p.first;
                                float ap = dis_p.second;
                                if ((dp < d_v && ap <= a_v) || (dp == d_v && ap < a_v))
                                    to_remove.push_back(dis_v);
                                if ((d_v < dp && a_v <= ap) || (d_v == dp && a_v < ap))
                                    to_remove.push_back(dis_p);
                            }
                            for (auto disp : to_remove)
                                L[v.id].erase(disp);
                            if (d_v < d[v])
                                d[v] = d_v;
                        }
                    }
                }
            }
        }
    }
    return L;
}

unordered_map<int, map<float, set<pair<float, int>>>>
shortest_fastest_distances::distances_eff_opt(float time, Nodes source) {
    unordered_map<int, map<float, set<pair<float, int>>>> AC;
    map<Nodes, set<pair<float, int>>> d;
    map<Nodes, set<pair<float, float>>> f;
    for (Nodes u : link_streams.nodes) {
        map<float, set<pair<float, int>>> acu;
        AC[u.id] = acu;
    }

    for (auto t : link_streams.time_set) {
        if (t >= time) {
            for (auto e : link_streams.temp_edges[t]) {
                Nodes u = e.u;
                Nodes v = e.v;
                float dur = e.dur;
                if (dur >= lambda) {
                    if (u == source) {
                        pair<float, int> source_dist(t, 0);
                        if (AC[u.id][t].find(source_dist) == AC[u.id][t].end())
                            AC[u.id][t].insert(pair<float, int>(t, 0));
                    }

                    if (AC[u.id].begin() != AC[u.id].end()) {
                        float su = AC[u.id].rbegin()->first;
                        auto itu = AC[u.id][su].rbegin();
                        float au = itu->first;
                        int du = itu->second;
                        if (au <= t and itu != AC[u.id][su].rend()) {
                            int dv = du + 1;
                            float av = t + lambda;
                            if (AC[v.id][su].find(pair<float, int>(av, dv)) ==
                                AC[v.id][su].end())//maybe remove this to save some time
                                AC[v.id][su].insert(pair<float, int>(av, dv));

                            vector<pair<float, int>> to_remove;
                            pair<float, int> dis_v(dv, av);

                            for (auto dis_p : AC[v.id][su]) {
                                int dp = dis_p.second;
                                float ap = dis_p.first;
                                if ((dp < dv && ap <= av) || (dp == dv && ap < av))
                                    to_remove.push_back(dis_v);
                                if ((dv < dp && av <= ap) || (dv == dp && av < ap))
                                    to_remove.push_back(dis_p);
                            }
                            for (auto rem : to_remove) {
                                AC[v.id][su].erase(rem);
                                if (AC[v.id][su].empty())
                                    AC[v.id].erase(su);
                            }

                            float fv = t + lambda - su;
                            if (f.find(v) != f.end() && !f[v].empty()) {
                                float fvp = f[v].rbegin()->second;
                                if (fvp < fv)
                                    fv = fvp;
                            }
                            f[v].insert(pair<float, float>(t + lambda, fv));

                            int f_dist = d.find(v) != d.end() ? d[v].rbegin()->second : 10 *
                                                                                        link_streams.nodes.size(); //la derniere mise a jour de la distance respectait la latence
                            int dmin = 10 * link_streams.nodes.size();
                            if (AC[v.id].find(su) != AC[v.id].end()) {
                                auto itv = AC[v.id][su].begin();
                                float av = itv->first;
                                float dv = itv->second;
                                if (av - su == fv)
                                    dmin = dv;
                            }
                            if (dmin < 10 * link_streams.nodes.size() && f_dist < dmin)
                                f_dist = dmin;
                            if (f_dist < 10 * link_streams.nodes.size())
                                d[v].insert(pair<float, int>(t, f_dist));
                        }
                    }
                }
            }
        }
    }

    return AC;
}

std::tuple<std::map<Nodes, std::set<std::pair<float, int>>>, std::map<Nodes, std::set<std::pair<float, float>>>, std::map<Nodes, std::map<float, std::set<std::pair<float, int>>>>>
shortest_fastest_distances::distances_eff(float time, Nodes source) {
    map<Nodes, map<float, set<pair<float, int>>>> AC;
    map<Nodes, set<pair<float, int>>> d;
    map<Nodes, set<pair<float, float>>> f;

    for (auto t : link_streams.time_set) {
        if (t >= time) {
            map<int, set<Nodes>> CC = link_streams.connected_components(t, -1);
            for (auto i_C_i : CC) {
                set<Nodes> C_i = i_C_i.second;
                map<Nodes, map<Nodes, int>> dist = link_streams.all_pairs_distances(t); // watch for INT_MAX

                //Construct set D
                map<float, set<pair<int, Nodes>>> D;
                if (C_i.find(source) != C_i.end()) {
                    pair<int, Nodes> p0(0, source);
                    set<pair<int, Nodes>> sp0;
                    sp0.insert(p0);
                    D[-t] = sp0;
                } else {
                    float max_sv = -1;
                    for (auto acit : AC) {
                        Nodes v = acit.first;
                        if (C_i.find(v) != C_i.end()) {
                            float sv = AC[v].rbegin()->first;
                            if (sv > max_sv)
                                max_sv = sv;
                        }
                    }
                    for (auto acit : AC) {
                        Nodes v = acit.first;
                        if (C_i.find(v) != C_i.end()) {
                            set<pair<int, Nodes>> D_v;
                            float sv = AC[v].rbegin()->first;
                            if (sv == max_sv) {
                                for (auto pdis_v : AC[v][sv]) {
                                    int dv = pdis_v.second;
                                    pair<int, Nodes> pair1(dv, v);
                                    D_v.insert(pair1);
                                }
                                D[-sv].insert(D_v.begin(), D_v.end());
                            }
                        }
                    }
                }

                for (auto trip_u : D) {
                    float su = trip_u.first;
                    for (auto trip_u_su : D[su]) {
                        int du = trip_u_su.first;
                        Nodes u = trip_u_su.second;

                        set<Nodes> U;
                        if (u == source) {
                            U.insert(u);
                        } else {
                            for (auto acit : AC) {
                                Nodes v = acit.first;
                                if (C_i.find(v) != C_i.end()) {
                                    for (auto map_v : acit.second) {
                                        if (map_v.first == -su) {
                                            for (auto pair_v_su : map_v.second) {
                                                if (du == pair_v_su.second) {
                                                    U.insert(v);
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        for (Nodes w : C_i) {
                            int d_start = 10 * link_streams.nodes.size();
                            if (AC.find(w) != AC.end() and AC[w].find(-su) != AC[w].end())
                                d_start = AC[w][-su].rbegin()->second;
                            int min_all_pairs = 10 * link_streams.nodes.size();

                            for (auto pair_nodes_all_pairs : dist) {
                                if (U.find(pair_nodes_all_pairs.first) != U.end()) {
                                    for (auto w_dist : pair_nodes_all_pairs.second) {
                                        if (w_dist.first == w) {
                                            int d_w = w_dist.second;
                                            if (d_w < min_all_pairs and d_w < 10 * link_streams.nodes.size())
                                                min_all_pairs = d_w;
                                        }
                                    }
                                }
                            }
                            int minval = min(du + min_all_pairs, d_start);
                            if (minval < 10 * link_streams.nodes.size()) {
                                assert(0 <= minval);
                                int dmin = minval;

                                //Remove dominated elements at
                                if (AC.find(w) != AC.end() and AC[w].find(-su) != AC[w].end()) {
                                    for (auto acit = AC[w][-su].begin(); acit != AC[w][-su].end();) {
                                        if (acit->first == t && acit->second > dmin) {
                                            acit = AC[w][-su].erase(acit);
                                        } else
                                            ++acit;
                                    }
                                }

                                assert(dmin >= 0);
                                if (AC.find(w) != AC.end()) {
                                    if (AC[w].find(-su) != AC[w].end())
                                        AC[w][-su].insert(pair<float, int>(t, dmin));
                                    else {
                                        set<pair<float, int>> set1;
                                        pair<float, int> pair1(t, dmin);
                                        set1.insert(pair1);
                                        AC[w][-su] = set1;
                                    }
                                } else {
                                    set<pair<float, int>> set1;
                                    pair<float, int> pair1(t, dmin);
                                    set1.insert(pair1);
                                    map<float, set<pair<float, int>>> map1;
                                    map1[-su] = set1;
                                    AC[w] = map1;
                                }

                                float fw_star = numeric_limits<float>::infinity();
                                if (f.find(w) != f.end())
                                    fw_star = f[w].rbegin()->second;
                                float fw = min(t + su, fw_star);
                                f[w].insert(pair<float, float>(t, fw));

                                float f_dist = numeric_limits<float>::infinity();
                                for (auto v_set : AC[w]) {
                                    float sp = v_set.first;
                                    float ap = v_set.second.begin()->first;
                                    int dp = v_set.second.begin()->second;
                                    if (ap - sp == fw && dp < f_dist)
                                        f_dist = dp;
                                }

                                assert(f_dist >= 0);
                                if (f_dist < numeric_limits<float>::infinity())
                                    d[w].insert(pair<float, int>(t, f_dist));
                            }
                        }
                    }
                }
            }
        }
    }

    return make_tuple(d, f, AC);
}

void shortest_fastest_distances::void_distances_eff(float time, Nodes source) {
    map<Nodes, map<float, set<pair<float, int>>>> AC;
    map<Nodes, set<pair<float, int>>> d;
    map<Nodes, set<pair<float, float>>> f;

    for (auto t : link_streams.time_set) {
        if (t >= time) {
            map<int, set<Nodes>> CC = link_streams.connected_components(t, -1);
            for (auto i_C_i : CC) {
                set<Nodes> C_i = i_C_i.second;
                map<Nodes, map<Nodes, int>> dist = link_streams.all_pairs_distances(t); // watch for INT_MAX

                //Construct set D
                map<float, set<pair<int, Nodes>>> D;
                if (C_i.find(source) != C_i.end()) {
                    pair<int, Nodes> p0(0, source);
                    set<pair<int, Nodes>> sp0;
                    sp0.insert(p0);
                    D[-t] = sp0;
                } else {
                    float max_sv = -1;
                    for (auto acit : AC) {
                        Nodes v = acit.first;
                        if (C_i.find(v) != C_i.end()) {
                            float sv = AC[v].rbegin()->first;
                            if (sv > max_sv)
                                max_sv = sv;
                        }
                    }
                    for (auto acit : AC) {
                        Nodes v = acit.first;
                        if (C_i.find(v) != C_i.end()) {
                            set<pair<int, Nodes>> D_v;
                            float sv = AC[v].rbegin()->first;
                            if (sv == max_sv) {
                                for (auto pdis_v : AC[v][sv]) {
                                    int dv = pdis_v.second;
                                    pair<int, Nodes> pair1(dv, v);
                                    D_v.insert(pair1);
                                }
                                D[-sv].insert(D_v.begin(), D_v.end());
                            }
                        }
                    }
                }

                for (auto trip_u : D) {
                    float su = trip_u.first;
                    for (auto trip_u_su : D[su]) {
                        int du = trip_u_su.first;
                        Nodes u = trip_u_su.second;

                        set<Nodes> U;
                        if (u == source) {
                            U.insert(u);
                        } else {
                            for (auto acit : AC) {
                                Nodes v = acit.first;
                                if (C_i.find(v) != C_i.end()) {
                                    for (auto map_v : acit.second) {
                                        if (map_v.first == -su) {
                                            for (auto pair_v_su : map_v.second) {
                                                if (du == pair_v_su.second) {
                                                    U.insert(v);
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        for (Nodes w : C_i) {
                            int d_start = 10 * link_streams.nodes.size();
                            if (AC.find(w) != AC.end() and AC[w].find(-su) != AC[w].end())
                                d_start = AC[w][-su].rbegin()->second;
                            int min_all_pairs = 10 * link_streams.nodes.size();

                            for (auto pair_nodes_all_pairs : dist) {
                                if (U.find(pair_nodes_all_pairs.first) != U.end()) {
                                    for (auto w_dist : pair_nodes_all_pairs.second) {
                                        if (w_dist.first == w) {
                                            int d_w = w_dist.second;
                                            if (d_w < min_all_pairs and d_w < 10 * link_streams.nodes.size())
                                                min_all_pairs = d_w;
                                        }
                                    }
                                }
                            }
                            int minval = min(du + min_all_pairs, d_start);
                            if (minval < 10 * link_streams.nodes.size()) {
                                assert(0 <= minval);
                                int dmin = minval;

                                //Remove dominated elements at
                                if (AC.find(w) != AC.end() and AC[w].find(-su) != AC[w].end()) {
                                    for (auto acit = AC[w][-su].begin(); acit != AC[w][-su].end();) {
                                        if (acit->first == t && acit->second > dmin) {
                                            acit = AC[w][-su].erase(acit);
                                        } else
                                            ++acit;
                                    }
                                }

                                assert(dmin >= 0);
                                if (AC.find(w) != AC.end()) {
                                    if (AC[w].find(-su) != AC[w].end())
                                        AC[w][-su].insert(pair<float, int>(t, dmin));
                                    else {
                                        set<pair<float, int>> set1;
                                        pair<float, int> pair1(t, dmin);
                                        set1.insert(pair1);
                                        AC[w][-su] = set1;
                                    }
                                } else {
                                    set<pair<float, int>> set1;
                                    pair<float, int> pair1(t, dmin);
                                    set1.insert(pair1);
                                    map<float, set<pair<float, int>>> map1;
                                    map1[-su] = set1;
                                    AC[w] = map1;
                                }

                                float fw_star = numeric_limits<float>::infinity();
                                if (f.find(w) != f.end())
                                    fw_star = f[w].rbegin()->second;
                                float fw = min(t + su, fw_star);
                                f[w].insert(pair<float, float>(t, fw));

                                float f_dist = numeric_limits<float>::infinity();
                                for (auto v_set : AC[w]) {
                                    float sp = v_set.first;
                                    float ap = v_set.second.begin()->first;
                                    int dp = v_set.second.begin()->second;
                                    if (ap - sp == fw && dp < f_dist)
                                        f_dist = dp;
                                }

                                assert(f_dist >= 0);
                                if (f_dist < numeric_limits<float>::infinity())
                                    d[w].insert(pair<float, int>(t, f_dist));
                            }
                        }
                    }
                }
            }
        }
    }
    AC.clear();
    d.clear();
    f.clear();
}

//TODO: CHECK IF I STILL HAVE TO REMOVE DOMINATED ELEMENTS
//TODO: CHECK IF INSERTIONS INTO F ARE CORRECT
//TODO: Test the following, with structures in the STL, instead of nested priority_queue (which looks complicated to implement)
//TODO: map<int, priority_queue<float>> VS -> map nodes to starting times (max-heap)
//TODO: map<int, map<float, priority_queue<int>>> VSD -> map nodes to starting times to distances (have to convert to min-heap)
//TODO: map<int, map<float, map<int, pair<float, float>>> AC -> as used currently
//TODO: This uses more space, however the accessors in map are constant time.
void shortest_fastest_distances::void_distances_eff_opt_new_DS(float time, Nodes source) {
    unordered_map<int, priority_queue<float>> VS;
    unordered_map<int, map<float, priority_queue<int, vector<int>, std::greater<int>>>> VSD;
    unordered_map<int, map<float, map<int, pair<float, float>>>> AC;
    map<Nodes, set<pair<float, int>>> d;
    map<Nodes, set<pair<float, float>>> f;
    for (Nodes u : link_streams.nodes) {
        map<float, map<int, pair<float, float>>> acu;
        AC[u.id] = acu;
    }

    for (auto t : link_streams.time_set) {
        if (t >= time) {
            for (auto e : link_streams.temp_edges[t]) {
                Nodes u = e.u;
                Nodes v = e.v;

                float dur = e.dur;
                if (dur >= lambda) {
                    if (u == source) {
                        VS[u.id].push(t);
                        VSD[u.id][t].push(0);
                        AC[u.id][t][0] = pair<float, float>(t, t);
                    }

                    if (!VS[u.id].empty()) {
                        float su = VS[u.id].top();
                        int du = VSD[u.id][su].top();
                        int dv = du + 1;
                        float av = t + lambda;

                        VS[v.id].push(su);
                        VSD[v.id][su].push(dv);
                        if (AC[v.id][su].find(dv) == AC[v.id][su].end())
                            AC[v.id][su][dv] = pair<float, float>(av, av);
                        else
                            AC[v.id][su][dv].second = av;

                        float fv = t + lambda - su;
                        if (f.find(v) != f.end() && !f[v].empty()) {
                            float fvp = f[v].rbegin()->second;
                            if (fvp < fv)
                                fv = fvp;
                        }
                        f[v].insert(pair<float, float>(t + lambda, fv));

                        int f_dist = d.find(v) != d.end() ? d[v].rbegin()->second : 10 * link_streams.nodes.size(); //la derniere mise a jour de la distance respectait la latence
                        int dmin = 10 * link_streams.nodes.size();
                        auto itv = AC[v.id][su].begin();
                        dv = itv->first;
                        av = itv->second.second; // MAYBE CHECK FOR ITV->SECOND.FIRST ALSO
                        if (av - su == fv)
                            dmin = dv;
                        if (dmin < 10 * link_streams.nodes.size() && f_dist < dmin)
                            f_dist = dmin;
                        if (f_dist < 10 * link_streams.nodes.size())
                            d[v].insert(pair<float, int>(av, f_dist));
                    }
                }
            }
        }
    }
}

void shortest_fastest_distances::distances_eff_opt_new_DS_alt(float time, Nodes source) {
    unordered_map<int, map<float, map<int, pair<float, float>>>> AC;
    map<int, set<pair<float, int>>> d;
    map<int, set<pair<float, float>>> f;
//    for (Nodes u : link_streams.nodes) {
//        map<float, map<int, pair<float, float>>> acu;
//        AC[u.id] = acu;
//    }

    for (auto t : link_streams.time_set) {
        if (t >= time) {
            for (auto e : link_streams.temp_edges[t]) {
                Nodes u = e.u;
                Nodes v = e.v;

                float dur = e.dur;
                if (dur >= lambda) {
                    if (u == source)
                        AC[u.id][t][0] = pair<float, float>(t, t);

                    if (AC[u.id].begin() != AC[u.id].end()) {
                        float su = AC[u.id].rbegin()->first;
                        int du = AC[u.id][su].begin()->first;
                        float au = AC[u.id][su].begin()->second.second;

                        if (au <= t) {
                            int dv = du + 1;
                            float av = t + lambda;
                            if (AC.find(v.id) == AC.end() or AC[v.id][su].find(dv) == AC[v.id][su].end())
                                AC[v.id][su][dv] = pair<float, float>(av, av);
                            else
                                AC[v.id][su][dv].second = av;

                            float fv =
                                    f.find(v.id) != f.end() and f[v.id].rbegin()->second < fv ? f[v.id].rbegin()->second
                                                                                              :
                                    t + lambda - su;
                            f[v.id].insert(pair<float, float>(t + lambda, fv));
                            int f_dist = d.find(v.id) != d.end() ? d[v.id].rbegin()->second : 10 *
                                                                                              link_streams.nodes.size(); //la derniere mise a jour de la distance respectait la latence
                            int dmin = 10 * link_streams.nodes.size();

                            dv = AC[v.id][su].begin()->first;
                            av = AC[v.id][su].begin()->second.second; // MAYBE CHECK FOR ITV->SECOND.FIRST ALSO
                            if (av - su == fv)
                                dmin = dv;
                            if (dmin < 10 * link_streams.nodes.size() && f_dist < dmin)
                                f_dist = dmin;
                            if (f_dist < 10 * link_streams.nodes.size())
                                d[v.id].insert(pair<float, int>(t, f_dist));
                        }
                    }
                }
            }
        }
    }
}


//Plus grand temps d'arrive u->v dont le temps de depart est s_uv
float shortest_fastest_distances::find_min_eat(map<pair<Nodes, Nodes>, map<float, set<tuple<float, float, int>>>> &D,
                                               pair<Nodes, Nodes> &puv, float s_uv) {
    float min_auv_prime = numeric_limits<float>::infinity(); //plus petit temps d'arrive u->v dont le temps de depart est plus grand que s_uv
    auto tprime = D[puv].begin();
    while (tprime != D[puv].end() and tprime->first <= s_uv) {
        ++tprime;
        for (auto s_d : tprime->second) {
            if (get<0>(s_d) > s_uv and get<1>(s_d) < min_auv_prime) {
                min_auv_prime = get<1>(s_d);
            }
        }
    }
    return min_auv_prime;
}

float shortest_fastest_distances::iter_time(float t, set<float>::reverse_iterator &time_it) {
    float tplus;
    if (time_it != link_streams.time_set.rbegin()) {
        time_it = prev(time_it);
        tplus = *time_it;
        ++time_it;
    } else
        tplus = t;
    return tplus;
}

std::tuple<std::map<std::pair<Nodes, Nodes>, std::map<float, std::map<float, int>>>, std::map<Nodes, AC_D_F_wrapper>>
shortest_fastest_distances::all_pairs_fas_distances_for(int max_time_units) {
    map<pair<Nodes, Nodes>, map<float, map<float, int>>> D;
    map<pair<Nodes, Nodes>, map<float, set<tuple<float, float, int>>>> D0;
    map<pair<Nodes, Nodes>, map<float, set<pair<float, float>>>> SA;
    map<pair<Nodes, Nodes>, map<float, float>> F;
    map<Nodes, AC_D_F_wrapper> finalwrapper;

    auto start = std::chrono::high_resolution_clock::now();
    chrono::duration<double> elapsed;

    for (float t : link_streams.time_set) {
        auto it = link_streams.time_set.lower_bound(t);
        --it;
        float tminus = t > *link_streams.time_set.begin() ? *it : t;

        map<Nodes, map<Nodes, int>> dist = link_streams.all_pairs_distances(t);
        map<int, set<Nodes>> CC = link_streams.connected_components(t, max_time_units);

        for (auto const &iter : CC) {
            set<Nodes> C = iter.second;

            for (Nodes u : C) {
                for (Nodes v : link_streams.nodes) {
                    pair<Nodes, Nodes> puv(u, v);
                    if (C.find(v) != C.end())
                        SA[puv][t].insert(make_pair(t, t));
                    else {
                        set<Nodes> C_v;
                        for (auto iter_v : CC)
                            if (iter_v.second.find(v) != iter_v.second.end())
                                C_v = iter_v.second;
                        float max_sv = -1;
                        for (Nodes w : C_v) {
                            pair<Nodes, Nodes> puw(u, w);
                            for (auto sa : SA[puw][tminus])
                                max_sv = max(max_sv, sa.first);
                        }
                        if (max_sv > -1) {
                            SA[puv][t].insert(make_pair(max_sv, t));
                            for (auto sa : SA[puv][tminus])
                                if (sa.first == max_sv)
                                    SA[puv][t].insert(make_pair(max_sv, sa.second));
                        }
                    }
                }
            }
            for (Nodes u : C) {
                for (Nodes v : link_streams.nodes) {
                    pair<Nodes, Nodes> puv(u, v);
                    if (C.find(v) != C.end())
                        D0[puv][t].insert(make_tuple(t, t, dist[u][v]));
                    else {
                        set<Nodes> C_v;
                        for (auto iter_v : CC)
                            if (iter_v.second.find(v) != iter_v.second.end())
                                C_v = iter_v.second;
                        for (auto sa_it : SA[puv][t]) {
                            int min_d = 10 * link_streams.nodes.size();
                            for (Nodes w : C_v) {
                                pair<Nodes, Nodes> puw(u, w);
                                int d_wv = dist[w][v];
                                int d_uw =
                                        D0[puw].find(tminus) != D0[puw].end() ? get<2>(*D0[puw][tminus].rbegin()) : 10 *
                                                                                                                    link_streams.nodes.size();
                                min_d = min(min_d, d_uw + d_wv);
                            }
                            if (min_d < 10 * link_streams.nodes.size())
                                D0[puv][t].insert(make_tuple(sa_it.first, t, min_d));

                            set<tuple<float, float, int>> dominated;
                            for (auto sad : D0[puv][t])
                                if (get<0>(sad) == sa_it.first and get<1>(sad) == t and get<2>(sad) > min_d)
                                    dominated.insert(sad);
                            for (auto d : dominated)
                                D0[puv][t].erase(d);
                        }
                    }
                    float l = F[puv].find(tminus) != F[puv].end() ? F[puv][tminus] : numeric_limits<float>::infinity();
                    set<pair<float, float>> psa;
                    for (auto sa : SA[puv][t]) {
                        l = min(l, sa.second - sa.first);
                        if (sa.second - sa.first == l)
                            psa.insert(make_pair(sa.first, sa.second));
                    }
                    F[puv][t] = l;
                    for (auto sa : psa)
                        for (auto sad : D0[puv][t]) {
                            if (get<0>(sad) == sa.first and get<1>(sad) == sa.second)
                                D[puv][t][sa.second] = get<2>(sad);
                        }
                }
            }
        }

        auto current = std::chrono::high_resolution_clock::now();
        auto elapsed = chrono::duration_cast<chrono::seconds>(current - start);
        int int_elapsed = chrono::duration<int>(elapsed).count();
        if (max_time_units > -1 and int_elapsed >= max_time_units)
            return make_tuple(D, map<Nodes, AC_D_F_wrapper>());
    }

    map<Nodes, AC_D_F_wrapper> mac;
    for (Nodes u : link_streams.nodes) {
        AC_D_F_wrapper ac(u, 0.0);
        mac[u] = ac; // No problem here
    }

    for (auto const kv : D0) {
        pair<Nodes, Nodes> puw = kv.first;
        Nodes u = puw.first;
        Nodes w = puw.second;
        map<float, set<pair<float, int >>> tmp_ac;
        for (auto t_v : kv.second) {
            for (auto triple : t_v.second) {
                float su = get<0>(triple);
                float au = get<1>(triple);
                float du = get<2>(triple);
                pair<float, float> paudu = make_pair(au, du);
                if (tmp_ac.find(su) != tmp_ac.end())
                    tmp_ac[su].insert(paudu);
                else
                    tmp_ac[su] = {paudu};
            }
        }
        mac[u].ac_dict[w].insert(tmp_ac.begin(), tmp_ac.end());
        assert(mac[u].ac_dict.size() > 0);
    }
    for (auto const kv : F) {
        pair<Nodes, Nodes> puw = kv.first;
        Nodes u = puw.first;
        Nodes w = puw.second;
        set<pair<float, float>> tmp_lat;
        for (auto av_l : kv.second)
            tmp_lat.insert(make_pair(av_l.first, av_l.second));
        mac[u].latencies[w].insert(tmp_lat.begin(), tmp_lat.end());
        assert(mac[u].latencies.size() > 0);
    }

    return make_tuple(D, mac);
}

unordered_map<int, map<float, map<int, pair<float, float>>>>
shortest_fastest_distances::distances_eff_opt_new_DS(float time, Nodes source) {
    unordered_map<int, map<float, map<int, pair<float, float>>>> AC;
    map<Nodes, set<pair<float, int>>> d;
    map<Nodes, set<pair<float, float>>> f;
    for (Nodes u : link_streams.nodes) {
        map<float, map<int, pair<float, float>>> acu;
        AC[u.id] = acu;
    }

    for (auto t : link_streams.time_set) {
        if (t >= time) {
            for (auto e : link_streams.temp_edges[t]) {
                Nodes u = e.u;
                Nodes v = e.v;
                float dur = e.dur;
                if (dur >= lambda) {
                    if (u == source)
                        AC[u.id][t][0] = pair<float, float>(t, t);

                    if (AC[u.id].begin() != AC[u.id].end()) {
                        float su = AC[u.id].rbegin()->first;
                        auto itu = AC[u.id][su].begin();
                        int du = itu->first; // SMALLEST DISTANCE FROM SOURCE TO U
                        float au = itu->second.second; // UPPER BOUND ON THE INTERVAL
                        if (au <= t and itu != AC[u.id][su].end()) {
                            int dv = du + 1;
                            float av = t + lambda;
                            if (AC[v.id][su].find(dv) == AC[v.id][su].end())
                                AC[v.id][su][dv] = pair<float, float>(av, av);
                            else
                                AC[v.id][su][dv].second = av;

                            float fv = t + lambda - su;
                            if (f.find(v) != f.end() && !f[v].empty()) {
                                float fvp = f[v].rbegin()->second;
                                if (fvp < fv)
                                    fv = fvp;
                            }
                            f[v].insert(pair<float, float>(t + lambda, fv));

                            int f_dist = d.find(v) != d.end() ? d[v].rbegin()->second : 10 *
                                                                                        link_streams.nodes.size(); //la derniere mise a jour de la distance respectait la latence
                            int dmin = 10 * link_streams.nodes.size();
                            auto itv = AC[v.id][su].begin();
                            dv = itv->first;
                            av = itv->second.second; // MAYBE CHECK FOR ITV->SECOND.FIRST ALSO
                            if (av - su == fv)
                                dmin = dv;
                            if (dmin < 10 * link_streams.nodes.size() && f_dist < dmin)
                                f_dist = dmin;
                            if (f_dist < 10 * link_streams.nodes.size())
                                d[v].insert(pair<float, int>(av, f_dist));
                        }
                    }
                }
            }
        }
    }
    return AC;
}

void shortest_fastest_distances::void_distances_wu(float time, Nodes source) {
    unordered_map<int, set<pair<int, float>>> L;
    map<Nodes, float> d;
    for (auto v : link_streams.nodes) {
        L[v.id] = set<pair<int, float>>();
        d[v] = v == source ? 0 : numeric_limits<float>::infinity();
    }

    for (auto t : link_streams.time_set) {
        if (t >= time) {
            for (auto e : link_streams.temp_edges[t]) {
                Nodes u = e.u;
                Nodes v = e.v;
                float dur = e.dur;
                if (dur >= lambda) {
                    if (u == source) {
                        pair<int, float> source_dist(0, t);
                        if (L[source.id].find(source_dist) == L[source.id].end())
                            L[source.id].insert(source_dist);
                    }

                    if (L[u.id].begin() != L[u.id].end()) {
                        auto it_dis_u = L[u.id].rbegin();

                        float au = it_dis_u->second;
                        int du = it_dis_u->first;
                        ++it_dis_u;
                        while (au > t and it_dis_u != L[u.id].rend()) {
                            au = it_dis_u->second;
                            du = it_dis_u->first;
                            ++it_dis_u;
                        }
                        if (au <= t) {
                            int d_v = du + 1;
                            float a_v = t + lambda;
                            pair<int, float> dis_v(d_v, a_v);
                            L[v.id].insert(dis_v);

                            vector<pair<int, float>> to_remove;
                            for (auto dis_p : L[v.id]) {
                                int dp = dis_p.first;
                                float ap = dis_p.second;
                                if ((dp < d_v && ap <= a_v) || (dp == d_v && ap < a_v))
                                    to_remove.push_back(dis_v);
                                if ((d_v < dp && a_v <= ap) || (d_v == dp && a_v < ap))
                                    to_remove.push_back(dis_p);
                            }
                            for (auto disp : to_remove)
                                L[v.id].erase(disp);
                            if (d_v < d[v])
                                d[v] = d_v;
                        }
                    }
                }
            }
        }
    }
}

void shortest_fastest_distances::void_all_pairs_fas_distances_posgam() {
    map<pair<int, int>, map<float, map<float, int>>> D;
    unordered_map<string, map<float, set<tuple<float, float, int>>>> D0;
    unordered_map<string, map<float, set<pair<float, float>>>> SA;
    map<pair<int, int>, map<float, float>> F;
    map<int, vector<int>> reached;
    int max_int = 10 * link_streams.nodes.size();

    for (auto s : reached)
        reached[s.first] = vector<int>();

    for (float t : link_streams.time_set) {
        auto it = link_streams.time_set.lower_bound(t);
        --it;
        float tminus = t > *link_streams.time_set.begin() ? *it : t;

        for (Edges e : link_streams.temp_edges[t]) {
            Nodes u = e.u;
            Nodes v = e.v;
            float dur = e.dur;

            if (dur >= lambda) {
                string spuv = to_string(u.id) + ":" + to_string(v.id);
                pair<int, int> puv(u.id, v.id);

                reached[v.id].push_back(u.id);
                SA[spuv][t] = set<pair<float, float>>{make_pair(t, t + lambda)};
                D0[spuv][t] = set<tuple<float, float, int>>{make_tuple(t, t + lambda, 1)};
                F[puv][t] = 0;

                for (int w : reached[u.id]) {
                    if (w != v.id) {
                        string spwu = to_string(w) + ":" + to_string(u.id);
                        string spwv = to_string(w) + ":" + to_string(v.id);
                        pair<int, int> pwu(w, u.id);
                        pair<int, int> pwv(w, v.id);

                        float tu = SA[spwu].rbegin()->first;
                        float max_su = SA[spwu][tu].rbegin()->first;
                        float t_lastu = D0[spwu].rbegin()->first;
                        int du = get<2>(*D0[spwu][t_lastu].rbegin());

                        int dv = max_int;
                        float t_lastv = t;
                        if (SA.find(spwv) != SA.end() and SA[spwv].rbegin()->first == t) {
                            SA[spwv][t].insert(make_pair(max_su, t + lambda));
                            t_lastv = D0[spwv].rbegin()->first;
                            dv = get<2>(*D0[spwv][t_lastv].rbegin());
                        } else
                            SA[spwv][t] = set<pair<float, float>>{make_pair(max_su, t + lambda)};

                        int dwv = min(dv, du + 1);
                        if (dwv < max_int) {
                            if (t_lastv == t)
                                D0[spwv][t].insert(make_tuple(max_su, t + lambda, dwv));
                            else
                                D0[spwv][t] = set<tuple<float, float, int>>{make_tuple(max_su, t + lambda, dwv)};
                        }

                        auto sa_wv = SA[spwv][t].rbegin();
                        float lwv = sa_wv->second - sa_wv->first;
                        float l = F[pwv].find(tminus) != F[pwv].end() ? F[pwv][tminus] : numeric_limits<float>::infinity();
                        l = min(lwv, l);
                        F[pwv][t] = l;
                        pair<float, float> sa_star(-1, -1);
                        for (auto sa : SA[spwv][t])
                            if (sa.second - sa.first == l) {
                                sa_star.first = sa.first;
                                sa_star.second = sa.second;
                            }
                        if (dwv < max_int and t_lastv == t) {
                            auto ds_wv = D0[spwv][t].rbegin();
                            while (ds_wv != D0[spwv][t].rend() and get<0>(*ds_wv) != sa_star.first and get<1>(*ds_wv) != sa_star.second)
                                ++ds_wv;
                            if (ds_wv != D0[spwv][t].rend())
                                D[pwv][t][sa_star.first] = get<2>(*ds_wv);
                        }
                    }
                }
            }
        }
    }
}

AC_D_F_wrapper::AC_D_F_wrapper() =
default;

AC_D_F_wrapper::AC_D_F_wrapper(Nodes
                               vp,
                               float
                               tvp) {
    v = vp;
    tv = tvp;
}

void AC_D_F_wrapper::filtertimeup(float t) {
    tv = t;

    for (auto ac_w : ac_dict) {
        Nodes w = ac_w.first;
        set<float> st_to_erase;
        set<float> at_to_erase;

        for (auto ac_w_sw : ac_w.second) {
            float sw = ac_w_sw.first;
            if (sw < t)
                st_to_erase.insert(sw);
            for (auto aw_dw : ac_w_sw.second)
                at_to_erase.insert(aw_dw.first);
        }
        for (auto it = distances[w].begin(); it != distances[w].end();) {
            if (at_to_erase.find(it->first) != at_to_erase.end())
                it = distances[w].erase(it);
            else
                ++it;
        }
        for (auto it = latencies[w].begin(); it != latencies[w].end();) {
            if (at_to_erase.find(it->first) != at_to_erase.end())
                it = latencies[w].erase(it);
            else
                ++it;
        }
        for (float st : st_to_erase)
            ac_dict[w].erase(st);
    }
}
