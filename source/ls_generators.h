//
// Created by Frédéric Simard on 18-08-29.
//

#ifndef UNTITLED_LS_GENERATORS_H
#define UNTITLED_LS_GENERATORS_H

#include "Link_Streams.h"
#include <random>

class ls_generators {
public:
    static Link_Streams generate_random_ls(int n, float a, float b, float p, unsigned int seed, bool real) {
        Link_Streams ls;

        std::default_random_engine gen_time(seed);
        std::default_random_engine gen_gnp(seed);
        std::uniform_real_distribution<float> dist_time_r(a, b);
        std::uniform_int_distribution<float> dist_time_i(a, b);
        std::uniform_real_distribution<float> dist_gnp(0.0, 1.0);

        for (int i = 0; i < n; i++) {
            for (int j = i+1; j < n; j++) {
                if (i != j and dist_gnp(gen_gnp) < p) {
                    float time = real ? dist_time_r(gen_time) : dist_time_i(gen_time);
                    ls.add_temporal_edge(Edges(time, i, j, 0));
                    ls.add_temporal_edge(Edges(time, j, i, 0));
                }
            }
        }
        // ls.add_ghost_edges();
        // ls.add_ghost_nodes();
        return ls;
    }

    static Link_Streams generate_random_ls_withdur(int n, float a, float b, float p, unsigned int seed, bool real) {
        Link_Streams ls;

        std::default_random_engine gen_time(seed);
        std::default_random_engine gen_gnp(seed);
        std::uniform_real_distribution<float> dist_time_r(a, b);
        std::uniform_int_distribution<float> dist_time_i(a, b);
        std::uniform_real_distribution<float> dist_gnp(0.0, 1.0);

        for (int i = 0; i < n; i++) {
            for (int j = i+1; j < n; j++) {
                if (i != j and dist_gnp(gen_gnp) < p) {
                    float time = real ? dist_time_r(gen_time) : dist_time_i(gen_time);
                    float dur;
                    if (real) {
                        std::uniform_real_distribution<float> dist_dur(0, b - time);
                        dur = dist_dur(gen_time);
                    } else {
                        std::uniform_int_distribution<float> dis_dur(0, b - time);
                        dur = dis_dur(gen_time);
                    }
                    ls.add_temporal_edge(Edges(time, i, j, dur));
                    ls.add_temporal_edge(Edges(time, j, i, dur));
                }
            }
        }
        return ls;
    }

    static Link_Streams generate_monotone(){
        Link_Streams ls;
        ls.add_temporal_edge(Edges(0,0,1,3));
        ls.add_temporal_edge(Edges(6,1,2,6));
        ls.add_temporal_edge(Edges(6,2,3,6));
        ls.add_temporal_edge(Edges(9,3,4,6));
        ls.add_temporal_edge(Edges(10,4,5,5));

        ls.add_temporal_edge(Edges(0,1,0,3));
        ls.add_temporal_edge(Edges(6,2,1,6));
        ls.add_temporal_edge(Edges(6,3,2,6));
        ls.add_temporal_edge(Edges(9,4,3,6));
        ls.add_temporal_edge(Edges(10,5,4,5));
        ls.add_ghost_edges();
        ls.add_ghost_nodes();
        return ls;
    }

    static Link_Streams generate_first(){
        Link_Streams ls = Link_Streams();
        ls.add_temporal_edge(Edges(0,0,1,1));
        ls.add_temporal_edge(Edges(3,0,2,1));
        ls.add_temporal_edge(Edges(6,2,3,1));
        ls.add_temporal_edge(Edges(9,1,3,1));
        ls.add_temporal_edge(Edges(12,3,4,1));

        ls.add_temporal_edge(Edges(0, 1, 0, 1));
        ls.add_temporal_edge(Edges(3, 2, 0, 1));
        ls.add_temporal_edge(Edges(6, 3, 2, 1));
        ls.add_temporal_edge(Edges(9, 3,1, 1));
        ls.add_temporal_edge(Edges(12, 4, 3, 1));

        ls.add_ghost_edges();
        ls.add_ghost_nodes();
        return ls;
    }
};


#endif //UNTITLED_LS_GENERATORS_H
