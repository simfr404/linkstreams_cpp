//
// Created by Frédéric Simard on 18-08-30.
//

#ifndef LINK_STREAMS_EXPERIMENTS_H
#define LINK_STREAMS_EXPERIMENTS_H


#include <vector>
#include <string>
#include "shortest_fastest_distances.h"

class experiments {
public:
    std::string sep = "\t";
    std::vector<int> nb_nodes;
    std::vector<float> up_times;
    std::vector<float> probs;
    std::vector<int> seeds;
    shortest_fastest_distances sf_engine;
    std::string input_file;
    std::string output_filename;

    experiments();

    experiments(std::vector<int> n, std::vector<float> b, std::vector<float> p, std::vector<int> s, std::string f);

    void run_randomgnp_exp();

    void run_randomgnp_exp_wusfpwallpairs(bool testvalues);

    void run_randomgnp_exp_wu_vs_allpairs();

    void run_betw_exp_realls(int rep_ex, int nbsources, int nbnodes);

    void run_inputls_exp(std::string inputfile, bool verbose, int verbose_num, int exp_mode, float input_lambda,
                         bool append, int repeat_exp, int nbsources, std::vector<int> sfd_methods);

    void run_random_avgdist(bool verbose, int verbose_num, bool append, int rep_exp);

    void run_lsgenerated_allpairs(std::string inputfile, bool verbose, int verbose_num, int exp_mode, float input_lambda, bool append, int repeat_exp, std::tuple<int, int, int> node_lb_ub,
                                      std::vector<int> sfd_methods, int ls_generator, std::pair<int, int> time_lb_ub, std::vector<float> gen_probs, bool realdurations);

    void run_random_real_ap(bool verbose, int vnum, bool append, int rep_ex);

    void print_fasdis(std::map<Nodes, std::map<Nodes, std::map<float, std::set<std::pair<float, int>>>>> &eff_dist,
                      std::map<Nodes, std::map<Nodes, std::map<float, std::set<std::pair<float, int>>>>> &ap_dist);

    void print_fastup(std::set<std::tuple<Nodes, Nodes, float, float, int>> &ef_t, std::set<std::tuple<Nodes, Nodes, float, float, int>> &ap_t);

    void initialize_exps(std::string inputfile, bool verbose, float input_lambda, bool append, int nbsources, Link_Streams & ls, std::ofstream & expfile, std::vector<Nodes> &sources_set);
};


#endif //LINK_STREAMS_EXPERIMENTS_H
