#include "shortest_fastest_distances.h"
#include "ls_generators.h"
#include "experiments.h"
#include "Link_Streams.h"
#include <chrono>
#include <regex>
#include <fstream>
#include <iostream>
#include <sstream>
#include <math.h>
#include <queue>
#include <assert.h>
#include <tuple>
#include <iomanip>

//All datasets are taken from the koblenz network collection http://konect.uni-koblenz.de/
const std::vector<std::string> networks{"slashdot-threads", "digg", "elec", "arxiv-hepph", "enron", "epinions-rating",
                                        "dblp", "facebook-wosn", "flickr-growth", "wikiconflict", "wikipedia-growth",
                                        "youtube-growth", "contact", "delicious_ui", "delicious_ut", "dnc",
                                        "lastfm_band", "lastfm_song", "lkml_person", "mit", "movielens",
                                        "munmun_twitter", "prosper_loans", "sociopatterns_hyper",
                                        "sociopatterns_infect"};
const std::vector<int> nb_nodes{70};
const std::vector<float> up_times{10};
const std::vector<float> probs{0.9};
const std::vector<int> seeds{1218, 124335};

const int exp_mode = 5;
const int suspect = 1;
const int numb_of_sources = 50000; //must ensure this number is high enough for small datasets
const int nb_of_probs = 50;
const std::string sep = "\t";

std::vector<std::string> define_net_set() {
    std::vector<std::string> exp_netws;

    switch (exp_mode) {
        case 0:
            for (int i = 0; i < 3; i++)
                exp_netws.push_back(networks[i]);
            break;
        case 1:
            for (int i = 0; i < 8; i++)
                exp_netws.push_back(networks[i]);
            break;
        case 2:
            exp_netws.push_back(networks[suspect]);
            break;
        case 3:
            exp_netws.push_back(networks[4]);
            exp_netws.push_back(networks[7]);
            exp_netws.push_back(networks[8]);
            break;
        case 4:
            for (int i = 19; i < networks.size(); i++)
                exp_netws.push_back(networks[i]);
            break;
        case 5: //small datasets
            exp_netws.push_back(networks[1]); //digg
            exp_netws.push_back(networks[2]); //elec
            exp_netws.push_back(networks[6]); //dblp
            exp_netws.push_back(networks[12]); //contact
            exp_netws.push_back(networks[15]); //dnc
            exp_netws.push_back(networks[20]); //movielens
            exp_netws.push_back(networks[23]); //socio
            exp_netws.push_back(networks[24]); //socio
            break;
        default:
            for (int i = 0; i < networks.size(); i++)
                exp_netws.push_back(networks[i]);
            break;
    }
    return exp_netws;
}

float f() {
    static int i = 1;
    return (float) i++ / (float) nb_of_probs;
}

void run_comparisons_on_datasets() {
    std::vector<std::string> exp_netws = define_net_set();

    std::cout << "--------------------" << std::endl;
    std::cout << "Launching experiment" << std::endl;
    float lambda = 0.5;
    experiments exper;
    auto start_tot = std::chrono::high_resolution_clock::now();
    for (std::string name : exp_netws) {
        std::vector<int> xps = {-1, 7};

        std::string out = "output/comparisons/results_" + name + "_" + std::to_string(numb_of_sources) + "-";
        for (int x : xps)
            out += "_" + std::to_string(x);
        out += ".csv";
        std::string in = "input/" + name + ".txt";

        std::cout << "Experiments on network " << name << std::endl;
        exper.output_filename = out;
        auto start = std::chrono::high_resolution_clock::now();

        exper.run_inputls_exp(in, true, 10, 3, lambda, true, 1, numb_of_sources, xps);

        auto finish = std::chrono::high_resolution_clock::now();
        std::chrono::duration<double> elapsed = finish - start;
        std::cout << "Duration of experiment: " << elapsed.count() << std::endl;
    }
    auto finish_tot = std::chrono::high_resolution_clock::now();
    std::chrono::duration<double> elapsed_tot = finish_tot - start_tot;
    std::cout << "Total duration of all experiments " << elapsed_tot.count() << std::endl;
}

void run_ap_scale_exp() {
    experiments ex;
    ex.output_filename = "output/ap_alone/ap_scale_sum.csv";
    ex.run_lsgenerated_allpairs("", true, 5, 0, 0.5, true, 5, std::make_tuple<int, int, int>(10, 200, 10),
                                std::vector<int>{2}, 1,
                                std::make_pair<int, int>(0, 10), std::vector<float>{0.7}, false);
}

void run_eff_vs_allp() {
    experiments ex;
    ex.output_filename = "output/ap_vs_eff/times_summary_eff_ap.csv";
    ex.run_lsgenerated_allpairs("", true, 5, 0, 0.5, false, 1, std::make_tuple<int, int, int>(100, 180, 10),
                                std::vector<int>{1,2}, 0,
                                std::make_pair<int, int>(0, 10), std::vector<float>{0.7}, false);
}

void run_ap_nul_vs_pos() {
    experiments ex;
    ex.output_filename = "output/ap_gampos_gamnul/times_summary_nul_vs_pos.csv";
    std::vector<float> p(nb_of_probs);
    std::generate(p.begin(), p.end(), f);
    ex.run_lsgenerated_allpairs("", true, 10, 3, 1.0, false, 5, std::make_tuple<int, int, int>(200, 201, 20),
                                std::vector<int>{2, 3}, 1,
                                std::make_pair<int, int>(0, 10),
                                p, false);
}

void get_datasets_stats() {
    std::string out = "output/datasets_stats.txt";
    std::ofstream outstream;
    outstream.open(out);

    outstream << std::right << std::setw(20) << "datasets name " << std::setw(20) << "number of nodes" << std::setw(20) << "number of times" << std::setw(20) << "number of edges " << std::endl;
    for (std::string name : networks) {
        std::string infile = "input/" + name + ".txt";
        Link_Streams ls;
        ls.read_koblenz_link_stream_from_file(infile, 0.5);
        int nbedges = 0;
        for (auto et : ls.temp_edges)
            nbedges += et.second.size();
        outstream << std::right << std::setw(20) << name << std::setw(20) << ls.nodes.size() << std::setw(20) << ls.time_set.size() << std::setw(20) << nbedges << std::endl;
    }

    outstream.close();
}

int main() {
    run_ap_scale_exp();
//    run_eff_vs_allp();
    run_ap_nul_vs_pos();
//    run_comparisons_on_datasets();
//    get_datasets_stats();

    return 0;
}
