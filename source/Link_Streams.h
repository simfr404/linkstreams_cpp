//
// Created by Frédéric Simard on 18-08-29.
//

#ifndef UNTITLED_LINK_STREAMS_H
#define UNTITLED_LINK_STREAMS_H


#include <vector>
#include <map>
#include <set>


class Nodes {
public:
    int id;

    Nodes();

    Nodes(int i);

    friend bool operator<(Nodes n1, Nodes n2);

    friend bool operator==(Nodes n1, Nodes n2);

    friend bool operator!=(Nodes n1, Nodes n2);

    friend std::ostream &operator<<(std::ostream &os, const Nodes &n);
};

class Edges {
public:
    float t;
    Nodes u;
    Nodes v;
    float dur;

    Edges(float time, Nodes nu, Nodes nv, float d);

    bool operator<(const Edges &e);

    bool operator==(const Edges &e1);

    friend std::ostream &operator<<(std::ostream &os, const Edges &e);
};


class Link_Streams {
public:
    char sep = ' ';
    std::set<std::pair<float, Nodes>> temp_nodes;
    std::set<Nodes> nodes;
    std::map<float, std::vector<Edges>> temp_edges;
    std::set<float> time_set;
    std::map<float, std::map<Nodes, std::set<Nodes>>> temp_adj;

    Link_Streams();

    void add_temporal_edge(Edges e);

    void add_node(Nodes n);

    void read_normal_link_stream_from_file(std::string filename);

    void read_koblenz_link_stream_from_file(std::string filename, float use_lambda);

    std::vector<float> split(const std::string &in_csv);

    //Unknown behaviour on directed graphs
    //Assumes graphs are undirected
    std::map<int, std::set<Nodes>> connected_components(float time, int max_time_unit);

    std::map<Nodes, std::map<Nodes, int>> all_pairs_distances(float time);

    virtual ~Link_Streams();

    void print_tempedges();

    void print_adjlist();

    void add_ghost_nodes();

    void add_ghost_edges();

    void add_adjac(const Edges &e, float time);
};

#endif //UNTITLED_LINK_STREAMS_H
