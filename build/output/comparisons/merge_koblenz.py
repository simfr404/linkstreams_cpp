import os
from pprint import pprint

merged = "merged_data_out_-1_7.csv"
datasets = ["slashdot-threads", "digg", "elec", "arxiv-hepph", "enron", "epinions-rating",
										"dblp", "facebook-wosn", "flickr-growth", "wikiconflict", "wikipedia-growth",
										"youtube-growth", "contact", "delicious_ui", "delicious_ut", "dnc",
										"lastfm_band", "lastfm_song", "lkml_person", "mit", "movielens",
										"munmun_twitter", "prosper_loans", "sociopatterns_hyper",
										"sociopatterns_infect"]
dir_path = os.path.dirname(os.path.realpath(__file__))
onfiles = [f for f in os.listdir(dir_path) if os.path.isfile(os.path.join(dir_path,f))]
header = "dataset;nb_sources;nb_nodes;nb_times;nb_edges;time;method;ratio\n"
len_header = 7
nb_col_to_read = len_header - 2

exps = ["50000-_-1_7"]

with open(merged, "a+") as m :
	m.write(header)
for files in onfiles:
	for dname in datasets:
		if dname in files:
			if [xp for xp in exps if xp in files]:
				nbsources = files.split("_")[2].split("-")[0]
				with open(files) as f:
					for line in f.readlines():
						if "nb_nodes" not in line:
							tmp = dname + ";" + nbsources + ";"
							normal_line = ' '.join(line.split())
							elements = normal_line.split(" ")
							if len(elements)>nb_col_to_read:
								for i in range(nb_col_to_read, len(elements)):
									elements[nb_col_to_read-1] += "_" + elements[i]
							for i in range(0, nb_col_to_read):
								el = elements[i]
								if i < nb_col_to_read-1:
									tmp += el + ";"
								else:
									tmp += el
							with open(merged, "a+") as m:
								m.write(tmp+"\n")

#compute ratios against wu
# newdata = []
# with open(merged, "r") as m:
# 	data = m.readlines()
# 	i = 1
# 	while(i < len(data)):
# 		if "time" not in data[i] and "wu" in data[i].split(";")[6]:
# 			j = i
# 			line = data[i].rstrip()
# 			dataset_name = line.split(";")[0]
# 			ratio = 0
# 			wu_time = float(line.split(";")[5])
			
# 			while(j<len(data) and data[j].split(";")[0] == dataset_name):
# 				newline = data[j].rstrip()
# 				time = float(data[j].split(";")[5])
# 				ratio = float(time) / float(wu_time)
# 				newline+= ";" + str(ratio) + "\n"
# 				newdata.append(newline)
# 				j+=1
# 			j-=1
# 			i = j
# 		else:
# 			i+=1
# pprint(newdata)

# with open(merged, "w") as m:
# 	m.write(header)
# 	for datum in newdata:
# 		m.write(datum)