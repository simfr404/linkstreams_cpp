import os

merged = "merged_data_out.csv"
datasets = ["arxiv", "dblp", "digg", "elec", "enron", "epinions", "facebook", "flickr", "slashdot", "wikiconflict", "wiki", "youtube"]

dir_path = os.path.dirname(os.path.realpath(__file__))
onfiles = [f for f in os.listdir(dir_path) if os.path.isfile(os.path.join(dir_path,f))]

with open(merged, "a+") as m :
	m.write("dataset;nb_sources;nb_nodes;nb_times;nb_edges;time_wu;time_eff \n")
for files in onfiles:
	for dname in datasets:
		if dname in files:
			nbsources = files.split("_")[2].split(".")[0]
			with open(files) as f:
				for line in f.readlines():
					if "nb_nodes" not in line:
						tmp = dname + ";" + nbsources + ";"
						elements = line.split("\t")
						for i in range(0, len(elements)):
							el = elements[i]
							if i < (len(elements) - 2):
								tmp += el + ";"
							else:
								tmp += el
						with open(merged, "a+") as m:
							m.write(tmp)