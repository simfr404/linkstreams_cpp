setwd("~/ClionProjects/LinkStreams/build/output/wu_vs_eff")

library("ggplot2")
library(rowr)
library(RColorBrewer)
library(reshape2)
library(xtable)
options("scipen" = 100, "digits" = 2)

dat = read.csv("merged_data_out.csv", sep=";", header=TRUE, dec=".")
dat = as.data.frame(dat)
dat$time_wu = signif(dat$time_wu, 3)
dat$time_eff = signif(dat$time_eff, 3)
dat$avg_deg = 2*dat$nb_edges/dat$nb_nodes
dat$avg_et = dat$nb_edges / dat$nb_times
dat$ratio = dat$time_eff / dat$time_wu

mlt = melt(dat, id.vars = "dataset", measure.vars = c("time_wu", "time_eff"))
mlt_d = melt(dat, id.vars = "avg_et", measure.vars = c("time_wu", "time_eff"))

q = ggplot(mlt, aes(x = dataset, y = value, colour = variable))
q = q + geom_point()
print(q)

q = ggplot(mlt_d, aes(x = avg_et, y = value, colour = variable))
q = q + geom_point()
#q = q + geom_smooth()
print(q)

mlt_r = melt(dat, id.vars = "avg_et", measure.vars = c("ratio"))
mlt_r = mlt_r[mlt_r$avg_et < 50000,]
q = ggplot(mlt_r, aes(x = avg_et, y = value))
q = q + geom_point()
q = q + geom_hline(yintercept=1, linetype="dashed", colour="red")
print(q)

mlt_et = melt(dat, id.vars = "dataset", measure.vars = c("ratio", "avg_et", "avg_deg"))
q = ggplot(mlt_et, aes(x = dataset, y = value, colour = variable))
q = q + geom_point()
print(q)

res = dat[c("dataset", "nb_nodes", "nb_times", "nb_edges", "avg_deg", "avg_et", "ratio")]
res = res[res$nb_edges >= 3000000,]
res$s = ifelse(res$ratio <= 10, 0, 1)
mlt_res = melt(res, id.vars = c("nb_edges", "nb_times"), measure.vars = c("s"))
g = ggplot(mlt_res, aes(x = nb_edges, y = nb_times, colour = value))
g = g + geom_point()
print(g)

res$om_e = res$nb_times * res$nb_edges

tab = dat[,c("dataset", "nb_nodes", "nb_times", "nb_edges", "time_wu", "time_eff", "ratio")]
colnames(tab) = c("Dataset", "$\\abs{V}$", "$\\abs{\\remt}$", "$\\abs{E_\\remt}$", "Runtime of Wu", "Runtime of \\ref{alg:forwpass}", "Runtime ratio")

xt = xtable(tab, caption = c("Runtime comparisons (in seconds) between Algorithms \\ref{alg:forwpass} and Wu et al.'s method"), label = c("tab:runtime_comp_eff_wu"), align = "lllll|rrr", digits=c(0,0,0,0,0,2,2,1))
print(xt, file="runtime_comp_wu_eff.tex", booktabs=TRUE, include.rownames=FALSE,sanitize.text.function=function(x){x})
