import os
from pprint import pprint

inp = "datasets_stats.txt"
out = "datasets_stats_tabs.txt"

with open(inp, "r") as f:
	for line in f.readlines():
		normal_line = ' '.join(line.split())
		elems = normal_line.split(" ")
		newline = ""
		for e in elems:
			newline += e + "\t"
		with open(out, "a+") as m:
			m.write(newline + "\n")
