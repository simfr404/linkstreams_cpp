READ_ME

AUTHOR: 
Frédéric Simard,

CURRENT AFFILIATION:
School of Electrical Engineering and Computer Science, 
University of Ottawa,
Ottawa, ON, Canada

EMAIL:
fsima063@uottawa.ca

This code was written for research purposes and is distributed as is with minimal cleanup.

The simplest way to build and run the program is the following:
	- Open a Terminal and change directory to "linkstreams_cpp"
	- use command "make"
	- change directory to "build" with "cd build"
	- run program with "./ls_metrics.out"
It is important to change directory to "build" as otherwise the paths to input datasets will not be recognized. The output files are written to "output". The attached makefile is minimal and can be configured as desired.

All sources files are in the main directory. Directory build/ contains an input/ and an output/ directory. The former includes the datasets used in the course of the experiments to compare with the literature. The latter directory contains results of the experiments. The statistical software R was used to analyze the results (mostly to build plots and tables) and the R files can be found in the output/ directory. 
Directory build/ can be built from standard commands "make". Thus, the program can be run by entering Terminal commands "make" and "./ls_metrics.out" in the built/ directory. Only small datasets (less than 10Mb) were pushed on GIT, the others can be found with in the Koblenz Network Collection http://konect.uni-koblenz.de/. Only minimal treatment was done on the datasets to convert them into readable format to the program, including in some cases converting the files to ".txt".

In order to help follow the experiments, the program contains many console outputs. It suffices to look for relevant "cout" commands to activate or deactivate those. The experiments are divided into multiple different experiments in order to make it easier to run separately. Some experiments rely on the datasets found in the directories input/, while others on the synthetic link streams constructed in "ls_generators.h". Note that "ls_generators.h" only contains static functions, thus its corresponding file "ls_generators.cpp" is empty. Some of those synthetic link streams are simply toy examples used for testing. Others are more complex and produce significant running times  when used as input to some methods. When an experiment is over, the output file can be found in either of output/ap_alone/, output/ap_vs_eff/ or output/wu_vs_eff/. The first directory contains experiments on the running time of MSMD (ap for all-pairs), the second one its comparison with SSMD while the third one contains results of the experiments on the comparison of running times between SSMD and Wu et al.'s method. 

The function to input datasets into link streams can be found in function "Link_Streams.cpp::read_koblenz_link_stream_from_file()". We suggest taking a look at this function to know the input format to the program. Furthermore, in the main file "main.cpp", the datasets names are declared on top in the vector "networks". We suggest either modifying this vector along with the function "define_net_set()" (that sets which of these datasets are to be used) or importing all datasets in the input directory.

The program was not originally designed to be as efficient as possible and much optimization is still possible. More comments on and in the program might be updated at a later date. If any questions arise, feel free to contact the author.

The included Python program "wrapper.py" is used to merge the outputs of the experiments into a single file.