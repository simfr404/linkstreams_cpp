import glob, os

def wrap_output_files(wrap_name):
	sep = "\t"
	outnames = ["slashdot-threads", "elec", "arxiv-hepph", "enron", "epinions-rating", "dblp",
                                      "digg", "facebook-wosn", "flickr-growth", "wikiconflict", "wikipedia-growth",
                                      "youtube-growth"]

	with open(wrap_name, "w+") as wfile:
		wfile.write("dataset \t nb nodes \t nb times \t nb edges \t time wu (s) \t time eff (s)\n")
		for f in glob.glob("./*.csv"):
			for dname in outnames:
				if dname in f:
					with open(f) as rfile:
						fname = f.split("_")[1]
						for line in [x.strip() for x in rfile]:
							if line != "" and "nb_nodes" not in line:
								wfile.write("\t".join([fname,line, "\n"]))


wrap_output_files("all_networks_results.txt")